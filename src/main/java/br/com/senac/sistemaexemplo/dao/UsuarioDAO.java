
package br.com.senac.sistemaexemplo.dao;

import br.com.senac.sistemaexemplo.model.Usuario;

public class UsuarioDAO extends DAO<Usuario>{
    
    public UsuarioDAO() {
        super(Usuario.class);
    }
    
}
